# Customers Posting Application

This is a small client to read data from CSV in resources, assign a unique ID and post to a Customer API. 

## How to Run

* Clone this repository
* Make sure you are using JDK 1.8 and Maven 3.x
* You can build the project by running ```mvn clean package```
* Once successfully built, you can run the service by one of these two methods:
```
        java -jar target/ReadCSV-0.0.1-SNAPSHOT.jar
or
        mvn spring-boot:run
```
# please ensure that the endpoint API at http://localhost:8080/customers is accessible before running the client
## About the Application

The applicatation is a loader client for posting requests to a Customers API. It is initialised through
commandline runner of spring boot and will exit after a successful run or an exception.
It will 
-load the a valid CSV in format from classpath
-read all lines with exact 3 values as we are loading only FirstName, LastName and email
-Return a list with unique id 
-Post each Object to end point

## Sample Data
The csv data is available in resources/data/Customer.txt