package com.test.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class ExternalConfig {
    @Value("${csvapplication.path}")
    private String filepath;

    @Value("${csvapplication.endpoint}")
    private String endpoint;

}
