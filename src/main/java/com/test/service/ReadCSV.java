package com.test.service;

import com.test.configuration.ExternalConfig;
import com.test.model.Customer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ReadCSV {

    private ExternalConfig externalConfig;
    private static AtomicInteger customerId = new AtomicInteger(1);     //Customer Id

    @Autowired
    public ReadCSV(ExternalConfig e) {
        this.externalConfig = e;
    }

    public ExternalConfig getConfig() {
        return externalConfig;
    }

    public List<Customer> readFile() {

        BufferedReader bufferedReader;
        List<Customer> customers = new ArrayList<>();
        try {
            //get the input stream
            ClassPathResource classPathResource = new ClassPathResource(externalConfig.getFilepath());
            bufferedReader = new BufferedReader(new InputStreamReader(classPathResource.getInputStream()));
            customers = bufferedReader.lines()
                    .skip(1)                                                 //skip the header
                    .map(line -> line.split(","))                            //split the lines using comma
                    .filter(linearray -> linearray.length == 3)                //ensure only valid records are inserted
                    .map(array -> new Customer(customerId.getAndIncrement(), array[0], array[1], array[2]))  //initiate the constructor and map to arrya
                    .collect(Collectors.toList());                           //collect the list
        } catch (IOException e) {
            log.error("Error Reading Input File");
        }

        return customers;
    }
}
