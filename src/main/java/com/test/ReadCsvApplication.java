package com.test;

import com.test.model.Customer;
import com.test.service.ReadCSV;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Slf4j
@EnableAutoConfiguration
@SpringBootApplication
public class ReadCsvApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReadCsvApplication.class, args);
    }

    @Bean
    public CommandLineRunner loadData(ReadCSV readCSV) {
        return (args) -> {
            log.info("Entering the Command Line Runner");
            List<Customer> customers = readCSV.readFile();

            String URL = readCSV.getConfig().getEndpoint();

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            try {
                customers.forEach(customer -> {
                    HttpHeaders headers = new HttpHeaders();
                    headers.setContentType(MediaType.APPLICATION_JSON);
                    HttpEntity<Customer> httpEntity = new HttpEntity<Customer>(customer, headers);
                    Customer response = restTemplate.postForObject(URL, customer, Customer.class);
                    log.info("Response Received--" + response.getId());

                });
            } catch (Exception e) {
                log.error(e.getMessage());
            }


        };
    }
}
